<?php
session_start();


$_SESSION['currentpage']=$_SERVER['PHP_SELF'];
if(!isset($_SESSION['currentuser'])){
	echo "<script>alert('Please login to access shopping cart.')</script>";

header('location: ' . $_SERVER['HTTP_REFERER']);
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="style.css" />
<link rel="stylesheet" type="text/css" href="tabmenu.css" />
<link rel="stylesheet" type="text/css" href="specialoffers.css" />
<title>Welcome to Elite Digital Solutions</title>
</head>
<script type="text/javascript">
<!--
    function toggle_visibility(id) {
       var e = document.getElementById(id);
       if(e.style.display == 'block')
          e.style.display = 'none';
       else
          e.style.display = 'block';
    }
//-->
</script> 
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script type="text/javascript" src="https:ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js"></script>
<script type="text/javascript">
function Slider(){
$(".slider #1").show("slide",{direction:'right'},5000);
$(".slider #1").delay(500).hide("slide",{direction: 'left'},5000);
var sc =$(".slider img").size();
var count= 2;

setInterval(function(){
	$(".slider #"+count).show("slide", {direction: 'right'},5000);
	$(".slider #"+count).delay(5000).hide("slide",{direction: 'left'},5000);
	
	if(count==sc){
		count=1
	}else{
		count= count+1
		}
		},15000);

	}



</script>
<body onload="Slider();">
<table border="0px" cellspacing="2px" cellpadding="2px" width="100%" id="header">
<tr>
<td></td>
<td>
<label class="header_text">Call Us: +254 20 375 3500-06 </label>  <label class="header_text">Email us: info@elitedigital.co.ke</label></td>
<td>
<?php
		// This code displays the current user who is logged into the website as
     if (isset($_SESSION['currentuser'])){
		
include'connect.php';
// This is selecting the user from the customers table in the database
$query= "SELECT * FROM customers "; 
	$result=mysql_query($query) or die (mysql_error());
	$row = mysql_fetch_array($result);
		echo "<label class='header_text'>Welcome ".$_SESSION['currentuser']."</label>";?>
      <a class="header_text" href="profile.php?profile=<?php echo $_SESSION['currentuser']; ?>""">View Profile</a></p>
      <p><?php echo "<br>";?>
        <a class="header_text"href="logout.php">Logout</a>
        <style type="text/css">
        #login{
display:none;
}
#reg{
display:none;
}
        </style>
        
      </p>
        <?php
		}else
		// if the user is not logged in, this is displayed
		echo "<label class='header_text'>Welcome Guest</label>";
		echo "<br>";
		?>
        <label ><a id="login"class="header_text" href="javascript:void(0)"onclick="toggle_visibility('popup-login');">Login</a> </label>
<label><a id="reg" class="header_text" href="javascript:void(0)"onclick="toggle_visibility('popup-reg');">Register </a></label>
</td>
</tr>
<tr>
<td>
<img src="Images/logo.jpg" />
</td>
<td>
<form method="post" action="search_results.php">
<label class="header_text">Search: </label>
<input type="text"  name="search" size="50"/>
<input type="submit" value=" Search"/> 

</form>
</td>
<td></td>
</tr>
</table>
<nav>
<div class="nav">
<ul>
<li> <a href="index.php" >Home</a></li>
<li> <a href="products.php">Products</a></li>
<Li> <a href="About_Us.php">About Us</a></li>
<li> <a href="Contact_Us.php">Contact Us</a></li>

</ul>
</div>
</nav>          
        
        <br/>
<table border="0" cellpadding="2px" cellspacing="2px">
<tr>
<td width="15%" id="sidebarnav" height="250px">
<div>
<h4 class="header_text">Categories</h4>
<li><a href="iPads.php" class="header_text">iPads</a></li>
<li><a href="iphones.php" class="header_text">iPhones</a></li>
<li><a href="ipods.php" class="header_text">iPods</a></li>
<li><a href="Macbooks.php" class="header_text">Mackbooks</a></li>
<li><a href="iMacs.php" class="header_text">iMacs</a></li>
<li><a href="Accessories.php" class="header_text">Accessories</a></li>
</div>
</td>
<td>
<h3>Shopping Cart</h3>

<h4>Order</h4>
<?php
include 'connect.php';
	$product_id = $_GET[id];	 //the product id from the URL 
	$action 	= $_GET[action]; //the action from the URL 

	//if there is an product_id and that product_id doesn't exist display an error message
	if($product_id && !productExists($product_id)) {
		die("Error. Product Doesn't Exist");
	}

	switch($action) {	//decide what to do	
	
		case "add":
			$_SESSION['cart'][$product_id]++; //add one to the quantity of the product with id $product_id 
		break;
		
		case "remove":
			$_SESSION['cart'][$product_id]--; //remove one from the quantity of the product with id $product_id 
			if($_SESSION['cart'][$product_id] == 0) unset($_SESSION['cart'][$product_id]); //if the quantity is zero, remove it completely (using the 'unset' function) - otherwise is will show zero, then -1, -2 etc when the user keeps removing items. 
		break;
		
		case "empty":
			unset($_SESSION['cart']); //unset the whole cart, i.e. empty the cart. 
		break;
	
	}
	
?>


<?php	

	if($_SESSION['cart']) {	//if the cart isn't empty
		//show the cart
		
		echo "<table border=\"1\" padding=\"3\" width=\"40%\">";	//format the cart using a HTML table
		
			//iterate through the cart, the $product_id is the key and $quantity is the value
			foreach($_SESSION['cart'] as $product_id => $quantity) {	
				
				//get the name, description and price from the database - this will depend on your database implementation.
				//use sprintf to make sure that $product_id is inserted into the query as a number - to prevent SQL injection
				$sql = sprintf("SELECT p_image,p_name, p_price, p_qty FROM products WHERE product_id = %d;",
								$product_id); 
					
				$result = mysql_query($sql);
					
				//Only display the row if there is a product (though there should always be as we have already checked)
				if(mysql_num_rows($result) > 0) {
			
					list($image, $name, $price,$qty) = mysql_fetch_row($result);
				
					$line_cost = $price * $quantity;		//work out the line cost
					$total = $total + $line_cost;			//add to the total cost
					$remain= $qty-$quantity;
					echo "<tr>";
					echo "<td><a href=\"$_SERVER[PHP_SELF]?action=remove&id=$product_id\"><img src='Images/remove.png'/></a>
						<a href=\"$_SERVER[PHP_SELF]?action=add&id=$product_id\"><img src='Images/add.png'/></a>
						</td>";
					echo "<td><img src='product_inventory/$image' width='100' height='100'/></td>";
						//show this information in table cells
						echo "<form method='post' action='mailorder.php' enctype='multipart/form-data'>
						 	   ";
						echo "<td><input type='text' value='$name' name='p_name'/></td>";
						//along with a 'remove' link next to the quantity - which links to this page, but with an action of remove, and the id of the current product
						echo "<td align=\"center\"><input type='text' value='$quantity' name='qty'/>						</td>";
						echo "<td align=\"center\"><input type= 'text' value='$price' name='price'/> 
						</td>";
						echo "<td align=\"center\"><input type= 'text' value='$line_cost' name='linecost'/> 
						</td>";
						echo "<input type='hidden' value='$remain' name='qty_remain'/>";
						
					
					echo "</tr>";
					
				}
			
			}
			
			//show the total
			echo "<tr>";
				echo "<td colspan=\"5\" align=\"right\"><strong>Total Ksh</strong></td>";
				echo "<td align=\"right\"><input type='text' value='$total' name='total'/>
				
				
				
				</td>";
			echo "</tr>";
			
			//show the empty cart link - which links to this page, but with an action of empty. A simple bit of javascript in the onlick event of the link asks the user for confirmation
			echo "<tr>";
				echo "<td colspan=\"6\" align=\"right\"><a href=\"$_SERVER[PHP_SELF]?action=empty\" onclick=\"return confirm('Are you sure?');\">Empty Cart</a>
				
				
				</td>";
			echo "</tr>";		
		echo "</table>";
		
		
	
	}else{
		//otherwise tell the user they have no items in their cart
		echo "You have no items in your shopping cart.";
		
	}
	
	//function to check if a product exists
	function productExists($product_id) {
			//use sprintf to make sure that $product_id is inserted into the query as a number - to prevent SQL injection
			$sql = sprintf("SELECT * FROM products WHERE product_id = %d;",
							$product_id); 
				
			return mysql_num_rows(mysql_query($sql)) > 0;
	}
?>

<a href="products.php">Continue Shopping</a>


<?php


?>
 <hr />

 <?php 
  $query="select * from customers WHERE email='$SESSION[currentuser]'";
	  $result=mysql_query($query) or die (mysql_error());
	?>
          <h4>Shipping Details of <?php echo $row['fname']?></h4>
      Address:<br /> <textarea cols="70" rows="10" name="address" ><?php echo $row['Address'];?>  </textarea><br />
      City: 
      <select name="city">
         <option value="<?php echo $row['City'];?>"></option>
        <option value="NBO">Nairobi</option>
      
        <option value="Msa">Mombasa</option>
      
        <option value="Kisimu">Kisimu</option>
       
        <option value="Eldoret">Eldoret</option>
    
        <option value="Nakuru">Nakuru</option>
     
        <option value="Naivasha">Naivasha</option>
     
      </select>
      <br />
     
      Postal/Zip Code: <br /> <textarea cols="70" rows="10" name="postal" ><?php echo $row['Zip_code'];?> </textarea><br />
      
     <hr />
       

<h4> Payment details of <?php echo $row['fname']?></h4>
Please select payment method:
          <select name="payment_method">
            <option value="Master Card">Master Card</option>
          
            <option value="Visa">Visa</option>
           
            <option value="PayPal">PayPal</option>
          
          </select><br />
          Credit Card Number:
          <input type="password" name="credit_card_no" value="<?php echo $row['credit_number'];?>  "/><br />
        
          Expiry Date:<input type="date" name="expiry_date" value="<?php echo $row['expiry_date'];?>  "/> <br />
          <input type='submit' value='check out' name='submit'/>
				</form>
</td>
</tr>
</table>
<!-- start popup login-->
<div id="popup-login" class="popup-position">
	<div id="popup-wrapper">
   
    	<div id="popup-container">
         <a id="header_text" href="javascript:void(0)"onclick="toggle_visibility('popup-login');"><img align="right" src="Images/close.png" /></a>
        <table border="0" cellspacing="2" cellpadding="2">
        <tr>
        <td>
        <h3> Customer Login</h3>
        <form method="post" action="customer_login.php">
        Email:<input type="email" name="email" /><br />
        Password:<input type="password" name="cpassword" /><br />
        <input type="submit" value="Login" name="clogin"/>
        </form></td>
        <td>
         <h3>Admin Login</h3>
        <form method="post" action="">
        Email:<input type="text" name="acusername" /><br />
        Password:<input type="password" name="apassword" /><br />
        <input type="submit" value="Login" name="alogin"/>
        </form></td>
        <tr>
        </table>
        </div>    <!-- end of popup container-->
    </div><!-- end login wrapper-->
</div><!-- end popup login-->

<!-- start popup reg-->
<div id="popup-reg" class="popup-position">
	<div id="popup-wrapper">
    	<div id="popup-container">
       <a id="header_text" href="javascript:void(0)"onclick="toggle_visibility('popup-reg');"><img align="right" src="Images/close.png" /></a>
        <h2> Sign-Up</h2></center>
        <form method="post" action="reg.php">
        <table> <tr><td>
          First Name:</td><td>
          <input type="text" name="fname"/></td></tr>
          <tr><td>
          Last Name:</td><td>
          <input type="text" name="lname" /></td></tr>
     		<tr><td>
          Date of Birth:</td><td><input type="date" name="DOB" /></td></tr>
         <tr><td>
          Email:</td>
          <td>
          <input type="email" name="email" /></td>
          </tr><td>
          Pasword:</td><td>
          <input type="password" name="password" /></td></tr>
        	<tr><td>
          Please select payment method:</td><td>
          <select name="payment_method">
            <option value="Master Card">Master Card</option>
          
            <option value="Visa">Visa</option>
           
            <option value="PayPal">PayPal</option>
          
          </select></td></tr>
          <tr><td>
          Credit Card Number:</td><td>
          <input type="text" name="credit_card_no" /></td>
         <tr><td>
          Expiry Date:</td><td><input type="date" name="expiry_date" /></td>
          <tr><td>
        
        <input type="submit" value="Register" name="submit" align="middle" /></td></tr></table>
        </form>
        </div>    <!-- end of popup container-->
    </div><!-- end reg wrapper-->
</div><!-- end popup reg--></td>

</table>
</body>
</html>